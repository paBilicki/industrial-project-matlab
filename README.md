# Industrial Project: “Characterization of 3G and LTE links on a geographical zone”#

Project that took place at TELECOM BRETAGNE in school year 2014/2015. The main goal was to characterize the real performance of 3G and 4G link in Rennes. The report contains the description of the whole process of the characterisation divided into few sections that correspond to the stages of the project. Starting with choosing parameters, and tools for tests and post processing gathered data, through the planning of the scenario of measurement campaign and ending with conclusions and future perspective of the subject.

** Keywords ** – *3G, 4G, evolution, performance, mobile networks, G-NetTrack, Wi2Me* 

## Repository ##
Contains the post processing tool used during the project.

### Input directory ###
Collected data during the measurement campaign with [G-NetTrack](http://www.gyokovsolutions.com/G-NetTrack%20Android.html) application 

### Output directory ###
* **comparer** - contains results of the analysis by time of a day, day of the week
* **handovers** - contains result of the handovers analysis

### Setting up ###
* The collected data must be put in the input directory
* The handovers.m script will generate the result in the output/handovers directory automatically without further configuration
* The comparer.m script will generate the results in the output/comparer/analyze# directory where # will be the number (1, 2 or 3) accordingly to the chosen type of the analysis that must be set up in order to run the script (1 by default)

### Version ###
This is the final version of the project.