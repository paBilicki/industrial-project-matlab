%% S5 PROJET GROUPE 902
% TELECOM Bretagne, Rennes annEe scolaire 2014-2015
% Authors: P. BILICKI, O. CHOJNACKA
% email: piotr.bilicki@telecom-bretagne.eu
% email: olga.chojnacka@telecom-bretagne.eu

%% CLEANING
close all; clear all; clc;

%% setting the folder with files to compare
% analyze = 1: 'mornings', 'afternoons', 'evenings', 'nights'
% analyze = 2: 'sunday', 'tuesday', 'saturday'
% analyze = 3: 'weekdays', all days all measurements compared '
analyze = 1;

if analyze == 1
    setOfFiles = {'mornings', 'afternoons', 'evenings', 'nights'};
elseif analyze == 2
    setOfFiles = {'Tuesday', 'Saturday', 'Sunday'};
elseif analyze == 3
    setOfFiles = {'weekdays'};
end
    
for elofset = 1:1:length(setOfFiles)
    foldername = setOfFiles{elofset};
    path = [pwd '\input\analysisday\' foldername '\'];
    listing = dir(path);

    % creating the handles for every file in the folder
    for i = 3 : 1: length(listing)
        fid(i-2) = fopen([path listing(i,1).name]);
    end

    %% PARSING FILES

    % PING 1, DOWNLOAD 2, UPLOAD 3
    TESTSTATUS = {'PING','DOWNLOAD','UPLOAD'};

    progressbar('Parsing file(s)');

    for f = 1:1:length(fid)
        tline = fgetl(fid(f));
        tline = fgetl(fid(f));
        index = 1;
        while ischar(tline)
            values = regexpi(tline,'\t','split');
            data(index).('DL_bitrate') = str2double(values {1,19});
            data(index).('UL_bitrate') = str2double(values {1,20});
            data(index).('PINGAVG') = str2double(values {1,45});
            data(index).('PINGMIN') = str2double(values {1,46});

            if strcmp(values{1,54},TESTSTATUS{1})
                data(index).('Test_Status') = 1;
            elseif strcmp(values{1,54},TESTSTATUS{2})
                data(index).('Test_Status') = 2;
            elseif strcmp(values{1,54},TESTSTATUS{3})
                data(index).('Test_Status') = 3;    
            else
                data(index).('Test_Status') = 0;
            end

            index = index + 1;
            tline = fgetl(fid(f));
        end
        fclose(fid(f));

        for i=1:1:length(data)
             DL_BITRATE(i,f) = data(1,i).DL_bitrate;
             UL_BITRATE(i,f) = data(1,i).UL_bitrate;
             PINGAVG(i,f) = data(1,i).PINGAVG;
             PINGMIN(i,f) = data(1,i).PINGMIN;
             TEST_STATUS(i,f) = data(1,i).Test_Status;
        end
        progressbar(f/length(fid));
    end

    DL = nan(length(data),length(fid));
    UL = nan(length(data),length(fid));
    PA = nan(length(data),length(fid));
    PM = nan(length(data),length(fid));

    for i=1:1:length(fid)
        coorDL = find(TEST_STATUS(:,i) == 2);
        DL(coorDL,i) = DL_BITRATE(coorDL,i);
        coorUL = find(TEST_STATUS(:,i) == 3);
        UL(coorUL,i) = UL_BITRATE(coorUL,i);

        coorP1 = find(TEST_STATUS(:,i) == 0);
        coorP2 = find(PINGMIN(:,i) > 0);
        coorP = intersect(coorP1, coorP2);
        PA(coorP,i) = PINGAVG(coorP,i);
        PM(coorP,i) = PINGMIN(coorP,i);
    end

    DLmbps = DL./1000;
    ULmbps = UL./1000;

    for i = 1:1:size(DLmbps,2)
        amountDL(i, 1) = sum(not(isnan(DLmbps(:,i))));
        amountUL(i, 1) = sum(not(isnan(DLmbps(:,i))));
        amountPA(i, 1) = sum(not(isnan(PA(:,i))));
        amountPM(i, 1) = sum(not(isnan(PM(:,i))));
    end

    %% PLOTS
    if analyze == 1

        %% THROUGHPUTS
        FIG1 = figure (1);
        BOX = boxplot(DLmbps, 'whisker',1000);
        title('Download bitrates of different weekdays','color', 'blue')
        ylabel('bitrate [Mbps]','color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,30]);
        for i=1:1:size(DLmbps,2)
            amount = num2str(amountDL(i,:));
            text(xtixloc(i), 27, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze1\' 'DL_' foldername ])

        FIG2 = figure (2);
        BOX = boxplot(ULmbps,'whisker',1000);
        title('Upload bitrates of different weekdays', 'color', 'blue')
        ylabel('bitrate [Mbps]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,8]);
        for i=1:1:size(ULmbps,2)
            amount = num2str(amountUL(i,:));
            text(xtixloc(i), 7, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze1\' 'UL_' foldername])

        %% PINGS
        FIG3 = figure (3);
        BOX = boxplot(PA, 'whisker',1000);
        title('Averages pings of different weekdays', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);

        ylim([-1, 1.1*max(max(PA))]);
        for i=1:1:size(PA,2)
            amount = num2str(amountPA(i,:));
            text(xtixloc(i), 20 + max(max(PA)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze1\' 'pingAVG_' foldername])

        FIG4 = figure (4);
        BOX = boxplot(PM, 'whisker',1000);
        title('Minimum pings of different weekdays', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 , 1.1*max(max(PM))]);
        for i=1:1:size(PM,2)
            amount = num2str(amountPM(i,:));
            text(xtixloc(i), 10 + max(max(PM)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze1\' 'pingMIN_' foldername])

    elseif analyze == 2
        %% THROUGHPUTS
        FIG1 = figure (1);
        boxplot(DLmbps, 'whisker',1000);
        title('Download bitrates on different times of the day', 'color', 'blue')
        ylabel('bitrate [Mbps]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'morning', 'afternoon', 'evening', 'night'};
        xtixloc = [1 2 3 4];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,30]);
        for i=1:1:size(DLmbps,2)
            amount = num2str(amountDL(i,:));
            text(xtixloc(i), 27, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze2\'  'DL_' foldername])

        FIG2 = figure (2);
        boxplot(ULmbps, 'whisker',1000);
        title('Upload bitrates on different times of the day', 'color', 'blue')
        ylabel('bitrate [Mbps]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'morning', 'afternoon', 'evening', 'night'};
        xtixloc = [1 2 3 4];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,8]);
        for i=1:1:size(ULmbps,2)
            amount = num2str(amountUL(i,:));
            text(xtixloc(i), 7, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze2\' 'UL_' foldername])

        %% PINGS
        FIG3 = figure (3);
        BOX = boxplot(PA,'whisker',1000);
        title('Averages pings on different times of the day', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'morning', 'afternoon', 'evening', 'night'};
        xtixloc = [1 2 3 4];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1, 1.1*max(max(PA))]);
        for i=1:1:size(PA,2)
            amount = num2str(amountPA(i,:));
            text(xtixloc(i), 20 + max(max(PA)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze2\' 'pingAVG_' foldername])

        FIG4 = figure (4);
        BOX = boxplot(PM, 'whisker',1000);
        title('Minimum pings on different times of the day', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'morning', 'afternoon', 'evening', 'night'};
        xtixloc = [1 2 3 4];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,1.1*max(max(PM))]);
        for i=1:1:size(PM,2)
            amount = num2str(amountPM(i,:));
            text(xtixloc(i), 10 + max(max(PM)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze2\' 'pingMIN_' foldername])

        elseif analyze == 3
        %% THROUGHPUTS
        FIG1 = figure (1);
        boxplot(DLmbps, 'whisker',1000);
        title('Download bitrates on different days', 'color', 'blue')
        ylabel('bitrate [Mbps]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,30]);
        for i=1:1:size(DLmbps,2)
            amount = num2str(amountDL(i,:));
            text(xtixloc(i), 27, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze3\' 'DL_' foldername])

        FIG2 = figure (2);
        boxplot(ULmbps, 'whisker',1000);
        title('Upload bitrates on different days', 'color', 'blue')
        ylabel('bitrate [Mbps]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,8]);
        for i=1:1:size(ULmbps,2)
            amount = num2str(amountUL(i,:));
            text(xtixloc(i), 7, ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze3\' 'UL_' foldername])

        %% PINGS
        FIG3 = figure (3);
        BOX = boxplot(PA,'whisker',1000);
        title('Averages pings on different days', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1, 1.1*max(max(PA))]);
        for i=1:1:size(PA,2)
            amount = num2str(amountPA(i,:));
            text(xtixloc(i), 20 + max(max(PA)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze3\' 'pingAVG_' foldername])

        FIG4 = figure (4);
        BOX = boxplot(PM, 'whisker',1000);
        title('Minimum pings on different days', 'color', 'blue')
        ylabel('time of response [ms]', 'color', 'blue')
        xlabel(foldername, 'color', 'blue')
        xtix = {'Tuesday', 'Saturday', 'Sunday'};
        xtixloc = [1 2 3];
        set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
        ylim([-1 ,1.1*max(max(PM))]);
        for i=1:1:size(PM,2)
            amount = num2str(amountPM(i,:));
            text(xtixloc(i), 10 + max(max(PM)), ['samples: ' amount], ...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
            'FontSize', 9, 'FontAngle', 'Italic')
        end
        print('-dpng','-r300',['output\comparer\analyze3\' 'pingMIN_' foldername])

    end
end