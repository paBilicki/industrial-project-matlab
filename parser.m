%% S5 PROJET GROUPE 902
% TELECOM Bretagne, Rennes ann�e scolaire 2014-2015
% Authors: P. BILICKI, O. CHOJNACKA
% email: piotr.bilicki@telecom-bretagne.eu
% email: olga.chojnacka@telecom-bretagne.eu


%% CLEANING
close all; clear all; clc;

%% SETTING PATH
path = [pwd '\input\analysisweekday\'];

setofdays = {'Tuesday.txt', 'Saturday.txt', 'Sunday.txt'};

for elofset = 1:1:3
filename = setofdays{elofset};
nameday = filename(1:length(filename)-4);
fid = fopen([path filename]);

%% PARSING FILE
% PING 1, DOWNLOAD 2, UPLOAD 3
TESTSTATUS = {'PING','DOWNLOAD','UPLOAD'};

tline = fgetl(fid);
tline = fgetl(fid);
index = 1;

Nrows = numel(textread([path filename],'%1c%*[^\n]'));
progressbar('Parsing file(s)');

while ischar(tline)
    values = regexpi(tline,'\t','split');
    data(index).('Timestamp') = datenum(values{1,1},'yyyy.mm.dd_HH.MM.SS');
    data(index).('Longitude') = str2double(values{1,2});
    data(index).('Latitude') = str2double(values{1,3});
    data(index).('Speed') = str2double(values{1,4});
    data(index).('CellID') = str2double(values{1,10});
    data(index).('LAC') = str2double(values{1,11});
    data(index).('NetworkTech') = values{1,12};
    data(index).('NetworkMode') = values{1,13};
    data(index).('Level') = str2double(values{1,14});
    data(index).('DL_bitrate') = str2double(values {1,19});
    data(index).('UL_bitrate') = str2double(values {1,20});
    data(index).('PINGAVG') = str2double(values {1,45});
    data(index).('PINGMIN') = str2double(values {1,46});
    data(index).('PINGMAX') = str2double(values {1,47});
    data(index).('PINGSTDEV') = str2double(values {1,48});
    data(index).('PINGLOSS') = str2double(values {1,49});
    data(index).('TESTDOWNLINK') = str2double(values {1,50});
    data(index).('TESTUPLINK') = str2double(values {1,51});
    data(index).('TESTDOWNLINKMAX') = str2double(values {1,52});
    data(index).('TESTUPLINKMAX') = str2double(values {1,53});
    
    if strcmp(values{1,54},TESTSTATUS{1})
        data(index).('Test_Status') = 1;
    elseif strcmp(values{1,54},TESTSTATUS{2})
        data(index).('Test_Status') = 2;
    elseif strcmp(values{1,54},TESTSTATUS{3})
        data(index).('Test_Status') = 3;    
    else
        data(index).('Test_Status') = 0;
    end
    
    index = index + 1;
    tline = fgetl(fid);
    progressbar(index/Nrows);
end
fclose(fid);

TIMESTAMP = nan(1,length(data));
CELLID = nan(1,length(data));
LEVEL = nan(1,length(data));
DL_BITRATE = nan(1,length(data));
UL_BITRATE = nan(1,length(data));
PINGAVG = nan(1,length(data));
PINGMIN = nan(1,length(data));
TEST_STATUS = nan(1,length(data));

% LONGITUDE = nan(1,length(data));
% LATITUDE = nan(1,length(data));
% SPEED = nan(1,length(data));
% LAC = nan(1,length(data));
% NETWORKTECH = nan(1,length(data));
% NETWORKMODE = nan(1,length(data));
% PINGMAX = nan(1,length(data));
% PINGSTDEV = nan(1,length(data));
% PINGLOSS = nan(1,length(data));
% TESTDOWNLINK = nan(1,length(data));
% TESTUPLINK = nan(1,length(data));
% TESTDOWNLINKMAX = nan(1,length(data));
% TESTUPLINKMAX = nan(1,length(data));

progressbar('Assigning variables');

for i=1:1:length(data)
    TIMESTAMP(i) = data(1,i).Timestamp;
    CELLID(i) = data(1,i).CellID;
    LEVEL(i) = data(1,i).Level;
    DL_BITRATE(i) = data(1,i).DL_bitrate;
    UL_BITRATE(i) = data(1,i).UL_bitrate;
    PINGAVG(i) = data(1,i).PINGAVG;
    PINGMIN(i) = data(1,i).PINGMIN;
    TEST_STATUS(i) = data(1,i).Test_Status;
%     LONGITUDE(i) = data(1,i).Longitude;
%     LATITUDE(i) = data(1,i).Latitude;
%     SPEED(i) = data(1,i).Speed;
%     CELLNAME(i) = data(1,i).Cellname;
%     LAC(i) = data(1,i).LAC;
%     NETWORKTECH(i) = data(1,i).NetworkTech;
%     PINGMAX(i) = data(1,i).PINGMAX;
%     PINGSTDEV(i) = data(1,i).PINGSTDEV;
%     PINGLOSS(i) = data(1,i).PINGLOSS;
%     TESTDOWNLINK(i) = data(1,i).TESTDOWNLINK;
%     TESTUPLINK(i) = data(1,i).TESTUPLINK;
%     TESTDOWNLINKMAX(i) = data(1,i).TESTDOWNLINKMAX;
%     TESTUPLINKMAX(i) = data(1,i).TESTUPLINKMAX;
    progressbar(i/length(data));
end

%% BOXPLOTS PREPARING
LEVEL(LEVEL == -200) = nan;
DL = nan(length(data), 1);
UL = nan(length(data), 1);
PINGS = nan(length(data), 3);

for i=1:1:length(TEST_STATUS)
    if TEST_STATUS(i) == 2
        DL(i) = DL_BITRATE(i);
    elseif TEST_STATUS(i) == 3
        UL(i) = UL_BITRATE(i);
    elseif isfloat(PINGAVG(i))
        PINGS(i, 1) = PINGMIN(i);
        PINGS(i, 2) = PINGAVG(i);
        %PINGS(i, 3) = PINGMAX(i);
    end
end
 table = [DL, UL]./1000;
 PINGS = [PINGMIN', PINGAVG'];
 
%% BOXPLOT signal level

coor90a = find(LEVEL > -100);
coor90b = find(LEVEL <= -90);
coor80a = find(LEVEL > -90);
coor80b = find(LEVEL <= -80);
coor70a = find(LEVEL > -80);
coor70b = find(LEVEL <= -70);
coor60a = find(LEVEL > -70);
coor60b = find(LEVEL <= -60);
coor50a = find(LEVEL > -60);
coor50b = find(LEVEL <= -50);

coor90 = intersect(coor90a, coor90b);
coor80 = intersect(coor80a, coor80b);
coor70 = intersect(coor70a, coor70b);
coor60 = intersect(coor60a, coor60b);
coor50 = intersect(coor50a, coor50b);

DL90 = nan(length(DL),1);
DL80 = nan(length(DL),1);
DL70 = nan(length(DL),1);
DL60 = nan(length(DL),1);
DL50 = nan(length(DL),1);

UL90 = nan(length(UL),1);
UL80 = nan(length(UL),1);
UL70 = nan(length(UL),1);
UL60 = nan(length(UL),1);
UL50 = nan(length(UL),1);

DL90(coor90) = DL(coor90);
DL80(coor80) = DL(coor80);
DL70(coor70) = DL(coor70);
DL60(coor60) = DL(coor60);
DL50(coor50) = DL(coor50);

UL90(coor90) = UL(coor90);
UL80(coor80) = UL(coor80);
UL70(coor70) = UL(coor70);
UL60(coor60) = UL(coor60);
UL50(coor50) = UL(coor50);

DLsignal = [DL90 DL80 DL70 DL60 DL50]./1000;
ULsignal = [UL90 UL80 UL70 UL60 UL50]./1000;
nrOfSamples = [length(coor90); length(coor80); length(coor70); length(coor60); length(coor50)];

%% PLOTS BITRATES

FIG1 = figure (1);
BOX = boxplot(DLsignal, 'whisker', 1000);
title(['Download bitrates as a function of received signal level - ' nameday], 'color', 'blue')
ylabel('bitrate [Mbps]', 'color', 'blue')
xlabel('Level of received signal [dBm]', 'color','blue')
xtix = {'-99 to -90 dBm','-89 to -80 dBm','-79 to -70 dBm','-69 to -60 dBm','-59 to -50 dBm'};
xtixloc = [1 2 3 4 5];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
ylim([-1 ,30]);
for i=1:1:size(DLsignal,2)
    amount = num2str(nrOfSamples(i,:));
    text(xtixloc(i), 27, ['samples: ' amount], ...
        'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 9, 'FontAngle', 'Italic')
end
print('-dpng','-r300',['output\parser\'  'DL_S_' nameday])

FIG2 = figure (2);
BOX = boxplot(ULsignal, 'whisker', 1000);
title(['Upload bitrates as a function of received signal level - ' nameday], 'color', 'blue')
ylabel('bitrate [Mbps]', 'color', 'blue')
xlabel('Level of received signal [dBm]', 'color','blue')
xtix = {'-99 to -90 dBm','-89 to -80 dBm','-79 to -70 dBm','-69 to -60 dBm','-59 to -50 dBm'};
xtixloc = [1 2 3 4 5];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
ylim([-1 ,9]);
for i=1:1:size(ULsignal,2)
    amount = num2str(nrOfSamples(i,:));
    text(xtixloc(i), 8, ['samples: ' amount], ...
        'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', 9, 'FontAngle', 'Italic')
end
print('-dpng','-r300',['output\parser\'  'UL_S_' nameday])

FIG3 = figure (3);
cdfplot(LEVEL)
title(['cdf graph of received signal level - ' nameday], 'color', 'blue')
xlabel('Level of received signal [dBm]', 'color', 'blue')
print('-dpng','-r300',['output\parser\'  'CDF_' nameday])

FIG4 = figure (4);
BOX = boxplot(table, 'whisker', 1000);
title(['Statistical overview of bitrates - ' nameday], 'color', 'blue')
ylabel('bitrate [Mbps]','color', 'blue')
xtix = {'download','upload'};
xtixloc = [1 2];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
print('-dpng','-r300',['output\parser\'  'overview_' nameday])

%%  PINGS PLOTS
FIG5 = figure (5);
BOX = boxplot(PINGMIN, 'whisker', 1000);
title(['Pings - ' nameday], 'color', 'blue')
ylabel('time of response [ms]', 'color', 'blue')
xtix = {'Minimum pings'};
xtixloc = [1];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
print('-dpng','-r300',['output\parser\' 'pingMIN_' nameday ])

FIG6 = figure (6);
BOX = boxplot(PINGAVG,  'whisker', 1000);
title(['Pings - ' nameday], 'color', 'blue')
ylabel('time of response [ms]', 'color', 'blue')
xtix = {'Average pings'};
xtixloc = [1];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
print('-dpng','-r300',['output\parser\' 'pingAVG_' nameday])

FIG7 = figure(7);
% [AX,F1,F2] = plotyy(TIMESTAMP,LEVEL,[TIMESTAMP',TIMESTAMP',TIMESTAMP'],[PINGMAX',PINGAVG',PINGMIN']);
[AX,F1,F2] = plotyy(TIMESTAMP,LEVEL,[TIMESTAMP',TIMESTAMP'],[PINGAVG',PINGMIN']);
set(get(AX(1),'Ylabel'),'String','Level of Received Signal [dBm]');
set(get(AX(2),'Ylabel'),'String','Pings [ms]');
set(F2(1), 'color', 'black', 'LineWidth', 1, 'LineStyle', 'o');
set(F2(2), 'color', 'green', 'LineWidth', 1, 'LineStyle', 'v');
xlabel('Timestamp') 
title(['Pings and Received Signal relation - ' nameday])
legend('level of signal','ping avg', 'ping min')
% print('-dpng','-r300',['output\parser\' 'pings_time_' nameday])
clear data;
end