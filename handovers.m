%% S5 PROJET GROUPE 902
% TELECOM Bretagne, ann�e scolaire 2014-2015
% Authors: P. BILICKI, O. CHOJNACKA

%% CLEANING
close all; clear all; clc;

%% setting the folder with files to compare
foldername = 'handovers';
path = [pwd '\input\' foldername '\'];
listing = dir(path);

% creating the handles for every file in the folder
for i = 3 : 1: length(listing)
    fid(i-2) = fopen([path listing(i,1).name]);
end

%% PARSING FILES

progressbar('Parsing file(s)');

for f = 1:1:length(fid)
    tline = fgetl(fid(f));
    tline = fgetl(fid(f));
    index = 1;
    while ischar(tline)
        
        values = regexpi(tline,'\t','split');
        data(index).('CellID') = str2double(values{1,10});
        
        index = index + 1;
        tline = fgetl(fid(f));
    end
    fclose(fid(f));

    for i=1:1:length(data)
         CELLID(i,f) = data(1,i).CellID;
    end
    progressbar(f/length(fid));
end

% deleting the lack of signal
CELLID(isnan(CELLID)) = 0;

% replecing the diff result, where was the change of the cellid, with 1
CELLID = not(not(diff(CELLID)));

% summing up the ones for every file
for i=1:1:length(fid);
    HO(1,i) = sum(CELLID(:,i));
end
HOD = nan(length(fid)/4,4);

HOD(1, 1:4) = HO(1:4);
HOD(2, 1:4) = HO(5:8);
HOD(3, 1:4) = HO(9:12);

FIG1 = figure (1);
bar(HOD)
xtix = {'Tuesday', 'Saturday', 'Sunday'};
ylabel('Number of handovers', 'FontSize', 14)
xtixloc = [1 2 3];
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc, 'FontSize', 14);
legend('mornings','afternoons', 'evenings', 'nights','Location','BestOutside','Orientation','horizontal')
colormap summer
print('-dpng','-r300',['output\handovers\' foldername])